<?php

namespace App\Http\Controllers;
use App\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;


class UsuarioController extends Controller
{
    
    public function getUsuarios()
    {
        //return "Si";
        $usuarios = Usuario::get();
        return ["usuarios"=>$usuarios];
    }

    public function saveUsuario(Request $request)
    {
        //return $request->all();
        $validator = \Validator::make($request->all(), [
            'nombres' => 'required|max:455',
            'apellidos' => 'required|max:455',
            'cedula' => 'required|numeric|unique:usuarios',
            'telefono' => 'required|numeric',
            'correo'=> 'required|max:255|unique:usuarios,correo|email',
        ],[
            'nombres.required' => 'El campo nombres es obligatorio.',
            'apellidos.required' => 'El campo apellidos es obligatorio.',
            'cedula.required' => 'El campo cédula es obligatorio.',
            'telefono.required' => 'El campo telefono es obligatorio.',
            'correo.required' => 'El campo correo es obligatorio.',
            'nombres.max' => 'El campo nombres debe ser máximo 455 caracteres.',
            'apellidos.max' => 'El campo apellidos debe ser máximo 455 caracteres.',
            'cedula.max' => 'El campo cédula debe ser máximo 50 caracteres.',
            'telefono.max' => 'El campo teléfono debe ser máximo 100 caracteres.',
            'correo.max' => 'El campo correo debe ser máximo 255 caracteres.',
            'correo.unique' => 'El correo electrónico ingresado ya está en uso.',
            'cedula.unique' => 'La cédula ingresada ya está en uso.',
            'cedula.numeric' => 'La cédula debe ser un valor numerico',
            'telefono.numeric' => 'El teléfono debe ser un valor numerico',
            ]);
        if($validator->fails()){
            return ["success"=>false,"errores"=>$validator->errors()->all()];
        }
        $usuario = new Usuario();
        $usuario->nombres = $request->nombres;
        $usuario->apellidos  = $request->apellidos;
        $usuario->cedula  = $request->cedula;
        $usuario->telefono  = $request->telefono;
        $usuario->correo  = $request->correo;
        
        $usuario->save();

        
        return ["success"=>true,"usuario"=>$usuario];
    }
    public function updateUsuario(Request $request)
    {
        //
        $validator = \Validator::make($request->all(), [
            'id' => 'required|exists:usuarios',
            'nombres' => 'required|max:455',
            'apellidos' => 'required|max:455',
            'telefono' => 'required|numeric',
            'cedula'=> 'required|numeric',
            'correo'=> 'required|max:255|email',
        ],[
            'nombres.required' => 'El campo nombres es obligatorio.',
            'apellidos.required' => 'El campo apellidos es obligatorio.',
            'cedula.required' => 'El campo cédula es obligatorio.',
            'telefono.required' => 'El campo telefono es obligatorio.',
            'correo.required' => 'El campo correo es obligatorio.',
            'nombres.max' => 'El campo nombres debe ser máximo 455 caracteres.',
            'apellidos.max' => 'El campo apellidos debe ser máximo 455 caracteres.',
            'cedula.max' => 'El campo cédula debe ser máximo 50 caracteres.',
            'telefono.max' => 'El campo teléfono debe ser máximo 100 caracteres.',
            'correo.max' => 'El campo correo debe ser máximo 255 caracteres.',
            'correo.email' => 'Formato del correo invalido.',
            'id.exists' => 'Favor recargar la página, el identificador del usuario no se encuentra',
            'cedula.numeric' => 'La cédula debe ser un valor numerico',
            'telefono.numeric' => 'El teléfono debe ser un valor numerico',
            ]);
        if($validator->fails()){
            return ["success"=>false,"errores"=>$validator->errors()->all()];
        }
        if(Usuario::where('correo',$request->correo)->where('id','<>',$request->id)->first() != null){
            return ["success"=>false,"errores"=>[["El correo ingresado ya se encuentra en uso."]]];
        }
        if(Usuario::where('cedula',$request->cedula)->where('id','<>',$request->id)->first() != null){
            return ["success"=>false,"errores"=>[["La cédula ingresada ya se encuentra en uso."]]];
        }

        $usuario = Usuario::where('id',$request->id)->first();
        $usuario->nombres = $request->nombres;
        $usuario->apellidos  = $request->apellidos;
        $usuario->cedula  = $request->cedula;
        $usuario->telefono  = $request->telefono;
        $usuario->correo  = $request->correo;
        
        $usuario->save();
        
        return ["success"=>true,"usuario"=>$usuario];
    }

}
