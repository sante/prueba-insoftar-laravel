<?php

use Illuminate\Database\Seeder;

class UsuariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('usuarios')->insert([
            'nombres' => 'Luis',
            'apellidos' => 'Palmera',
            'cedula' => '123456',
            'telefono' => '123456',
            'correo' => 'luis@gmail.com',
        ]);
    }
}
